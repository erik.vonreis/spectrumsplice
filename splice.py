import numpy as np
import re
import sys

def read_sweep_freqs(sweep_file):
    """
    Read in sweep freqs from file object. Return as list of freqs in Hz


    :param sweep_file_name:
    :return:
    """
    freqs = []
    for raw_line in sweep_file.readlines():
        line = raw_line.split("#",1)[0].strip()
        try:
            f = int(line)
            freqs.append(f)
        except ValueError:
            pass
    return freqs


time_series_re = re.compile(r'Time Series')


def comment_split(line):
    """
    Split a line of text into before and after '#' comment
    :param line:
    :return: A tuple with the first item the stripped string before the first '#'
    the second, the stripped string after the first '#', including any subsequent '#'
    """
    comment_split = line.split("#", 1)
    before_comment = comment_split[0].strip()
    if len(comment_split) > 1:
        after_comment = comment_split[1].strip()
    else:
        after_comment = ""
    return (before_comment, after_comment)

class FreqFile:
    def __init__(self, frequency_Hz):
        self.nominal_frequency_Hz = frequency_Hz

        self.file_name = None
        self.date = None
        self.center_freq_Hz = None
        self.sample_rate_Hz = None
        self.frame_len = None
        self.data = []

        self.valid = False

        self.load()

    def load(self):
        """
        Load the data from a frequency file
        fills in class members
        :return:
        """
        self.file_name = f"{self.nominal_frequency_Hz}.txt"
        try:
            with open(self.file_name, "rt") as f:
                raw_lines = f.readlines()
        except IOError as e:
            print(f"Could not open frequency file {self.file_name}: {e}")
            return

        state = "lead"

        while len(raw_lines):
            line = raw_lines[0]
            raw_lines = raw_lines[1:]
            split_line = comment_split(line)

            if state == "lead":
                if time_series_re.match(split_line[1]):
                    state = "header"

                    line = raw_lines[0]
                    raw_lines = raw_lines[1:]
                    split_line = comment_split(line)
                    self.date = split_line[1]
                else:
                    print(f"could not recognize file type in file {self.file_name}")
                    return

            elif state == "header":
                if split_line[1] == "Center Freq:":

                    line = raw_lines[0]
                    raw_lines = raw_lines[1:]
                    split_line = comment_split(line)

                    try:
                        self.center_freq_Hz = float(split_line[0])
                        if self.center_freq_Hz != self.nominal_frequency_Hz:
                            print(f"Center freq read from file does not match file name freq: {self.center_freq_Hz} != {self.nominal_frequency_Hz}")
                            return
                    except ValueError:
                        print(f"Could not read center freq. from freq file {self.file_name}")
                        return
                elif split_line[1] == "Sample Rate:":

                    line = raw_lines[0]
                    raw_lines = raw_lines[1:]
                    split_line = comment_split(line)

                    try:
                        self.sample_rate_Hz = float(split_line[0])
                    except ValueError:
                        print(f"Could not read sample rate from freq file {self.file_name}")
                        return

                elif split_line[1] == "Number of Points:":

                    line = raw_lines[0]
                    raw_lines = raw_lines[1:]
                    split_line = comment_split(line)

                    try:
                        self.frame_len = float(split_line[0])
                    except ValueError:
                        print(f"Could not read number of points from freq file {self.file_name}")
                        return

                elif split_line[1][:4] == "----":
                    state = "body"

                    if self.center_freq_Hz is None:
                        print(f"Did not find center frequency in {self.file_name}")
                        return
                    if self.sample_rate_Hz is None:
                        print(f"Did not find center frequency in {self.file_name}")
                        return
                    if self.frame_len is None:
                        print(f"Did not find center frequency in {self.file_name}")
                        return

                    line = raw_lines[0]
                    raw_lines = raw_lines[1:]
                    split_line = comment_split(line)

                    # check that headers are what we expect
                    headers = split_line[1].split()
                    if headers[0] != "Real" or headers[2] != "Imaginary":
                        print(f"Unknown headers at top of data.  Found '{headers.join(',')}'")
                        return

            elif state == "body":
                strvals = split_line[0].split()
                if len(strvals) == 2:
                    try:
                        real_val = float(strvals[0])
                        imag_val = float(strvals[1])
                        self.data.append(np.array([real_val, imag_val]))
                    except ValueError:
                        pass

        if len(self.data) != self.frame_len:
            print(f"length of data did not match specified frame length in {self.file_name}")
            return

        self.valid = True
        self.data = np.array(self.data)

    @property
    def bandwidth_Hz(self):
        return self.sample_rate_Hz / self.frame_len

    @property
    def start_freq_Hz(self):
        return self.center_freq_Hz - (self.sample_rate_Hz/2)

    @property
    def end_freq_Hz(self):
        """actually equal to bandiwidth + highest bucket"""
        return self.center_freq_Hz + (self.sample_rate_Hz/2)

    def full_data(self):
        """
        Return data with some anciallary calculations: freq, magnitude, phase
        :return:
        """

        freq_Hz = np.arange(self.start_freq_Hz, self.end_freq_Hz, self.bandwidth_Hz)

        real = self.data[:,0]
        imag = self.data[:,1]

        mag = (real**2 + imag **2) ** 0.5

        phase_rad = np.arctan2(imag, real)

        out = np.transpose(np.array([freq_Hz, real, imag, mag, phase_rad]))

        return out

    def find_overlap(self, next):
        """
        Return how many points of overlap exist between
        :param next:
        :return:
        """
        if self.bandwidth_Hz != next.bandwidth_Hz:
            print("Can't calculate overlap between frames with different bandwidths")
            return 0

        return int((self.end_freq_Hz - next.start_freq_Hz) / self.bandwidth_Hz + 0.5)


def write_spectrum(freq_files, output_file = "outfile.csv"):
    """
    Take a list of FreqFiles and write out a combined spectrum
    as a CSV file
    :param freq_files:
    :return:
    """

    max_freq = 0

    with open(output_file, "wt") as f:
        f.write("Freq. (Hz),Real (I),Imag. (Q),Magnitude,Phase (rad)\n")



        for i in range(len(freq_files)):
            freq_file = freq_files[i]
            full_data = freq_file.full_data()

            if i + 1 < len(freq_files):
                overlap = freq_file.find_overlap(freq_files[i+1])
                if overlap > 0:
                    full_data = full_data[:-(overlap//2)]
                else:
                    print(f"Warning: no overlap between files {freq_file.file_name} and {freq_files[i+1].file_name}")

            bigger = [ row[0] > max_freq for row in full_data]

            if len(bigger) > 0:
                max_freq = full_data[-1][0]

            for line in full_data[bigger]:
                f.write(",".join([str(v) for v in line]) + "\n")


def run():
    try:
        with open("Sweep_Frequencies.txt", "rt") as sweep_file:
            freqs = read_sweep_freqs(sweep_file)
        freqs.sort()

        freq_files=[]

        for freq in freqs:
            # plus 100k because that's how it is
            freqfile = FreqFile(freq + 100000)
            if freqfile.valid:
                freq_files.append(freqfile)
            else:
                print(f"{freq} file is invalid")

        write_spectrum(freq_files)

    except IOError as e:
        print(f"Cannot find file 'Sweep_Frequencies.txt': {e}")
        return


if __name__ == "__main__":
    run()