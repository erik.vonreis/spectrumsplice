# Setup

You will need python 3.7 or later

You will also need the numpy package

# Use

Copy the splice.py script into the directory that contains Sweep_Frequencies.txt and the related spectrum 'NNNNN.txt' files.

open a command prompt.

Run ```python3 splice.py```.  This will output "outfile.csv", which can be opened in a spread sheet.

Output calculates magnitude and phase.  The first line is a column header.

# Notes

Since the edges of each patch of spectrum are filtered, since the filtering is symmetrical on the left and right sides, 
and since each block overlaps the next, the splice point is such that each frame contributes an equal amount to the 
overlap.
